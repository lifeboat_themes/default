"use strict";

$('.slider').each(function (e) {
  var slider = $(this),
      min_display = slider.find("*[data-role='min']"),
      max_display = slider.find("*[data-role='max']"),
      min_val = slider.find("*[data-role='min_val']"),
      max_val = slider.find("*[data-role='max_val']"),
      min = parseInt(slider.data("min")),
      max = parseInt(slider.data("max")),
      step = (max - min) / 100;
  slider.slider({
    'range': true,
    'values': [min, max],
    'min': min,
    'step': step,
    'minRange': step,
    'max': max,
    'slide': function slide(event, ui) {
      min_display.html(ui.values[0]);
      max_display.html(ui.values[1]);
      min_val.val(ui.values[0]);
      max_val.val(ui.values[1]);
    }
  });
});

//# sourceMappingURL=price-slider.js.map