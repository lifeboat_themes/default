"use strict";

(function ($) {
  "use strict";

  $(".slick").slick({
    arrows: false,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    }, {
      breakpoint: 775,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2
      }
    }, {
      breakpoint: 440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  var slick_vertical_slider = $('.slick-vertical'),
      slick_vertical_controls = $('.slick-vertical-sidebar');
  slick_vertical_slider.slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    infinite: false,
    prevArrow: $('.slick-navigation .prev'),
    nextArrow: $('.slick-navigation .next'),
    asNavFor: slick_vertical_controls
  });
  slick_vertical_controls.slick({
    vertical: true,
    verticalSwiping: true,
    slidesToShow: 8,
    slidesToScroll: 1,
    arrows: false,
    infinite: false,
    dots: false,
    centerMode: false,
    responsive: [{
      breakpoint: 579,
      settings: {
        slidesToShow: 7,
        slidesToScroll: 1
      }
    }]
  });
  slick_vertical_controls.find(".slick-slide").click(function (e) {
    e.preventDefault();
    slick_vertical_slider[0].slick.slickGoTo(parseInt($(this).index()));
  }); // Full Width

  $(".full-width-slick").slick({
    arrows: false,
    dots: false,
    infinite: false,
    autoplay: true,
    autoplaySpeed: 2500,
    speed: 2500,
    slidesToShow: 1,
    slidesToScroll: 1
  }); // Small

  $(".slick-small").slick({
    arrows: false,
    dots: false,
    infinite: false,
    speed: 300,
    slidesToShow: 4,
    slidesToScroll: 1,
    responsive: [{
      breakpoint: 1260,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 775,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }, {
      breakpoint: 440,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }]
  });
  $(window).resize();
})(jQuery);

//# sourceMappingURL=slick-sliders.js.map