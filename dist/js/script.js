"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var ProductForm = function ProductForm(form, container) {
  "use strict";

  var _this = this;

  this.make_select_field = function (option, opts) {
    var elem = $("<select>", {
      'class': "form-control",
      'id': option,
      'name': option
    });
    opts.forEach(function (item) {
      elem.append($("<option>", {
        'value': item
      }).html(item));
    });
    return elem;
  };

  this.make_select = function (option, name, options) {
    var select_options = [];
    options.forEach(function (item) {
      select_options.push($("<option>", {
        'value': item
      }).html(item));
    });
    return $("<div>", {
      'class': "mt-4"
    }).html([$("<label>", {
      'for': option
    }).html(name + ":"), _this.make_select_field(option, options)]);
  };

  this.render_price = function () {
    var c = container.find(".price-container").html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
    var btn = form.find('.btn-checkout').addClass('disabled').prop('disabled', true);
    Lifeboat.Product.CalculatePrice(form.data("id"), _this.get_variant_data(), 1, function (data) {
      if (data.StockAmount === 0) {
        c.html("<strong class='text-danger'>Out of Stock</strong>");
      } else {
        var sell_price = "<h3>" + data.SellingPrice + "</h3>";

        if (data.isDiscounted) {
          c.html("<del><i>" + data.BasePrice + "</i></del>" + sell_price + "<span class='badge bade-danger'>" + data.DiscountPercent + "% OFF</span>");
        } else {
          c.html(sell_price);
        }

        btn.removeClass('disabled').prop('disabled', false);
      }
    });
  };

  this.render_variants = function () {
    var v = $("<div>", {
      'class': "variant-container"
    });
    v.html('<i class="fa fa-spinner fa-spin fa-2x"></i>');
    Lifeboat.Product.Variants(form.data("id"), function (data) {
      v.html("");

      if (_typeof(data) === 'object' && data !== null) {
        for (var x = 1; x < 4; x++) {
          var opt_name = 'Option' + x;

          if (data[opt_name]) {
            v.append(_this.make_select('Option' + x, data[opt_name].Name, Object.keys(data[opt_name].Options)));
          }

          v.find("select#" + opt_name).change(function () {
            var $this = $(this),
                elem_id = $this.attr('id'),
                val = $this.val();

            var _loop = function _loop(y) {
              var check_opt = 'Option' + y;
              if (elem_id === check_opt) return "continue";
              var select = v.find("select#" + check_opt);
              var allow = false;
              var curr = select.val();
              data[elem_id].Options[val][check_opt].forEach(function (item) {
                if (item === curr) allow = true;
              });

              if (!allow) {
                select.val(data[elem_id].Options[val][check_opt][0]);
              }
            };

            for (var y = 1; y < 4; y++) {
              var _ret = _loop(y);

              if (_ret === "continue") continue;
            }

            _this.render_price();
          });
        }

        $(v.find("select")[0]).trigger("change");
      }

      _this.render_price();
    });
    return v;
  };

  this.render_quantity_control = function () {
    var input = $("<input>", {
      type: "text",
      "class": "form-control",
      name: "Quantity",
      value: 1,
      id: "Quantity"
    }),
        dec_button = $("<button>", {
      type: "button",
      "class": "btn btn-alternative hover-bg-alternative",
      "aria-label": "Decrease quantity by 1"
    }).html("-"),
        inc_button = $("<button>", {
      type: "button",
      "class": "btn btn-alternative hover-bg-alternative",
      "aria-label": "Increase quantity by 1"
    }).html('+'),
        field = $("<div>", {
      'class': "mt-4"
    }).html([$("<label>", {
      'for': "quantity"
    }).html("Quantity:"), $("<div>", {
      'class': "input-group"
    }).html([dec_button, input, inc_button])]);
    dec_button.click(function (e) {
      e.preventDefault();
      var v = parseInt(input.val()) - 1;
      input.val(v < 2 ? 1 : v);
    });
    inc_button.click(function (e) {
      e.preventDefault();
      input.val(parseInt(input.val()) + 1);
    });
    return field;
  };

  this.get_variant_data = function () {
    return {
      "Option1": form.find(".form-control#Option1").val(),
      "Option2": form.find(".form-control#Option2").val(),
      "Option3": form.find(".form-control#Option3").val()
    };
  };

  this.init = function () {
    var qty_field = _this.render_quantity_control();

    container.append($("<div>", {
      "class": "price"
    }).append([$("<div>", {
      "class": "price-container"
    }), _this.render_variants(), qty_field]));
    form.find(".add-to-cart").click(function (e) {
      e.preventDefault();
      Lifeboat.Cart.AddItem(form.data("id"), _this.get_variant_data(), qty_field.find("#Quantity").val(), function () {
        location.reload();
      });
    });
  };
};

(function ($) {
  "use strict";

  $(".bg-img").parent().addClass("bg-size");
  jQuery(".bg-img").each(function () {
    var elem = $(this),
        src = elem.attr("src"),
        parent = elem.parent();
    parent.css({
      "background-image": "url(" + src + ")",
      "background-size": "auto 100%",
      "background-position": "center",
      "display": "block"
    });
    elem.hide();
  });
  $("#subscribe-form .input-group-prepend").on("click", function (e) {
    $(this).parents("form").submit();
  });
  $('#subscribe-form').on('submit', function (e) {
    e.preventDefault();
    var $this = $(this),
        url = $this.data("url"),
        response_box = $("#subscribe-form-response");
    $.ajax({
      url: url,
      data: {
        "email": $this.find("#subscribe-email").val()
      },
      method: "get",
      success: function success() {
        response_box.removeClass('alert alert-danger').addClass('alert alert-success').html('Thank you for subscribing');
      },
      error: function error(response) {
        response_box.removeClass('alert alert-success').addClass('alert alert-danger').html(response.responseJSON['error']);
      }
    });
  });
  $(window).on('scroll', function () {
    if ($(this).scrollTop() > 600) {
      $('#scroll-top').fadeIn();
    } else {
      $('#scroll-top').fadeOut();
    }
  });
  $('#scroll-top').on('click', function () {
    $("html, body").animate({
      scrollTop: 0
    }, 600);
    return false;
  });
  $('.loader-wrapper').fadeOut('slow', function () {
    $(this).remove();
  });
  $("*[data-role='product']").each(function () {
    var elem = $(this);
    elem.find("*[data-role='quantity']").change(function () {
      Lifeboat.Cart.UpdateQuantity(elem.data('id'), elem.data('variant'), $(this).val(), function () {
        location.reload();
      });
    });
    elem.find("*[data-role='delete']").click(function (e) {
      e.preventDefault();
      Lifeboat.Cart.RemoveItem(elem.data('id'), elem.data('variant'), function () {
        location.reload();
      });
    });
  });
  $("*[data-role='product-form']").each(function () {
    var pf = new ProductForm($(this), $($(this).data('form')));
    pf.init();
  });
  $(".wishlist-toggle").click(function (e) {
    e.preventDefault();
    Lifeboat.WishList.Toggle($(this).data("id"), function () {
      location.reload();
    });
  });
  $(".search-page .search-sidebar .btn-close").click(function (e) {
    e.preventDefault();
    $(".search-page .search-sidebar").addClass("collapsed");
  });
  $(".search-page .search-open").click(function (e) {
    e.preventDefault();
    $(".search-page .search-sidebar").removeClass("collapsed");
  });
})(jQuery);

(function () {
  var frame = document.getElementById('map_frame_container');
  var key = document.getElementById('map_frame_key');
  var search = document.getElementById('map_frame_query');
  if (!frame || !key || !search) return this;
  var map = document.createElement('iframe');
  map.src = 'https://www.google.com/maps/embed/v1/place?q=' + search.value.replace(' ', '+') + '&key=' + key.value;
  map.style.border = "0";
  map.style.height = "100%";
  map.style.width = "100%";
  map.setAttribute('allow', 'fullscreen');
  map.setAttribute('loading', 'lazy');
  frame.appendChild(map);
})(jQuery);

//# sourceMappingURL=script.js.map