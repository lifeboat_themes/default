<section class="contact-page">
    <div class="col-lg-8 offset-lg-2">
        <div class="row">
            <div class="col-lg-6">
                <form role="form" action="$AbsoluteLink" method="post" data-role="ajax-form">
                    <div class="col-md-12">
                        <div class="form-group mb-4">
                            <label for="name">Name</label>
                            <input type="text" class="form-control br-0" id="name" placeholder="Enter Your name" required="required" name="name">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-4">
                            <label for="email">Email</label>
                            <input type="email" class="form-control br-0" id="email" placeholder="Email" required="required" name="email">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="review">Write Your Message</label>
                            <textarea class="form-control br-0" placeholder="Write Your Message" id="message" name="message" rows="2" required="required"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 text-right">
                        <button class="btn btn-color hover-bg-color mt-4 br-0" type="submit">
                            <i class="fa fa-envelope mr-1"></i> SEND MESSAGE
                        </button>
                    </div>
                    $IncludeReCaptchaField('contact')
                </form>
            </div>
            <div class="col-lg-5 offset-lg-1 bg-color text-white p-5 static-contact">
                <div class="col-md-12 mb-5"><h2 class="text-white">Contact Us Directly</h2></div>
                <div class="col-md-12 mb-4">
                    <% if $Email %>
                        <i class="fa fa-envelope-o mr-2"></i> $Email <br/>
                    <% end_if %>
                    <% if $Tel %>
                        <i class="fa fa-phone mr-2"></i> $Tel
                    <% end_if %>
                </div>
                <% if $Address %>
                    <div class="col-md-12 mb-4">
                        <i class="fa fa-map-marker mr-2"></i> <strong>Address:</strong><br />
                        $Address
                    </div>
                <% end_if %>
                <% if $OpeningHours %>
                    <div class="col-md-12">
                        <i class="fa fa-clock-o mr-2"></i> <strong>Opening Hours:</strong><br />
                        $OpeningHours
                    </div>
                <% end_if %>
            </div>
        </div>
    </div>
</section>
<% if $Theme.CustomField('MapLocation').Value %>
    <div class="pb-4" id="map_frame_container" style="height:500px;">
        <input type="hidden" id="map_frame_query" value="$Theme.CustomField('MapLocation').Value" />
        <input type="hidden" id="map_frame_key" value="AIzaSyAWr-_Q_0Eg08q76abBeV3uhjj4jvYBif4" />
    </div>
<% end_if %>